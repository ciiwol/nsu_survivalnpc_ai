﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace survivalNPC_AI
{
    /// <summary>
    /// 콘솔 폼 클래스.
    /// 싱글톤으로 구현하여 어디서든 static write메소드를 호출할 수 있게한다.
    /// </summary>
    public partial class frmConsole : Form
    {
        static frmConsole instance;

        public static frmConsole getInstance
        {
            get
            {
                if (instance == null)
                    instance = new frmConsole();
                return instance;
            }
        }

        delegate void writeCallback(string str, bool line);

        public frmConsole()
        {
            InitializeComponent();
            txtConsole.Text += "Conosole Started." + Environment.NewLine;
        }

        private void frmEvolveState_Load(object sender, EventArgs e)
        {
        }

        public static void write(string str, bool line)
        {
            TextBox txt = getInstance.txtConsole;
            if (txt.InvokeRequired)
            {
                txt.Invoke(new writeCallback(write), new object[] { str, line });
            }
            else
            {
                txt.Text += str;
                if (line) txt.Text += Environment.NewLine;
                if (getInstance.chkAutoScroll.Checked)
                {
                    txt.SelectionStart = txt.Text.Length;
                    txt.ScrollToCaret();
                }
            }
        }

        private void btnConsoleClear_Click(object sender, EventArgs e)
        {
            txtConsole.Text = "";
        }
    }
}
