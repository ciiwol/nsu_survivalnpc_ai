﻿namespace survivalNPC_AI
{
    partial class frmConsole
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtConsole = new System.Windows.Forms.TextBox();
            this.pnDock = new System.Windows.Forms.Panel();
            this.chkAutoScroll = new System.Windows.Forms.CheckBox();
            this.btnConsoleClear = new System.Windows.Forms.Button();
            this.pnDock.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtConsole
            // 
            this.txtConsole.BackColor = System.Drawing.Color.Black;
            this.txtConsole.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtConsole.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtConsole.ForeColor = System.Drawing.Color.Lime;
            this.txtConsole.Location = new System.Drawing.Point(0, 24);
            this.txtConsole.Multiline = true;
            this.txtConsole.Name = "txtConsole";
            this.txtConsole.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtConsole.Size = new System.Drawing.Size(259, 264);
            this.txtConsole.TabIndex = 0;
            // 
            // pnDock
            // 
            this.pnDock.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnDock.Controls.Add(this.chkAutoScroll);
            this.pnDock.Controls.Add(this.btnConsoleClear);
            this.pnDock.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnDock.Location = new System.Drawing.Point(0, 0);
            this.pnDock.Name = "pnDock";
            this.pnDock.Size = new System.Drawing.Size(259, 24);
            this.pnDock.TabIndex = 1;
            // 
            // chkAutoScroll
            // 
            this.chkAutoScroll.AutoSize = true;
            this.chkAutoScroll.Checked = true;
            this.chkAutoScroll.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAutoScroll.Location = new System.Drawing.Point(3, 3);
            this.chkAutoScroll.Name = "chkAutoScroll";
            this.chkAutoScroll.Size = new System.Drawing.Size(85, 16);
            this.chkAutoScroll.TabIndex = 1;
            this.chkAutoScroll.Text = "Scroll Auto";
            this.chkAutoScroll.UseVisualStyleBackColor = true;
            // 
            // btnConsoleClear
            // 
            this.btnConsoleClear.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnConsoleClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConsoleClear.ForeColor = System.Drawing.Color.Lime;
            this.btnConsoleClear.Location = new System.Drawing.Point(188, 0);
            this.btnConsoleClear.Name = "btnConsoleClear";
            this.btnConsoleClear.Size = new System.Drawing.Size(69, 22);
            this.btnConsoleClear.TabIndex = 0;
            this.btnConsoleClear.Text = "Clear";
            this.btnConsoleClear.UseVisualStyleBackColor = true;
            this.btnConsoleClear.Click += new System.EventHandler(this.btnConsoleClear_Click);
            // 
            // frmConsole
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(259, 288);
            this.ControlBox = false;
            this.Controls.Add(this.txtConsole);
            this.Controls.Add(this.pnDock);
            this.ForeColor = System.Drawing.Color.Lime;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmConsole";
            this.ShowIcon = false;
            this.Text = "Console";
            this.Load += new System.EventHandler(this.frmEvolveState_Load);
            this.pnDock.ResumeLayout(false);
            this.pnDock.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtConsole;
        private System.Windows.Forms.Panel pnDock;
        private System.Windows.Forms.Button btnConsoleClear;
        private System.Windows.Forms.CheckBox chkAutoScroll;
    }
}