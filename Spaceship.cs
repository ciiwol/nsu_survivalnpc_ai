﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace survivalNPC_AI
{
    public class Spaceship
    {
        #region public 속성
        public int x
        {
            get { return X; }
            set
            {
                if (value < 0) X = 0;
                else if (value >= gm.fieldSize[0] - width) X = gm.fieldSize[0] - width - 1;
                else X = value;
            }
        }
        public int y
        {
            get { return Y; }
            set
            {
                if (value < 0) Y = 0;
                else if (value >= gm.fieldSize[1] - height) Y = gm.fieldSize[1] - height - 1;
                else Y = value;
            }
        }

        public int width = 30;
        public int height = 30;

        public bool mov_U = false;
        public bool mov_R = false;
        public bool mov_D = false;
        public bool mov_L = false;
        public float mov_U_Value = 0;
        public float mov_R_Value = 0;
        public float mov_D_Value = 0;
        public float mov_L_Value = 0;

        public int speed = 4;
        #endregion

        #region private 속성
        Image img;
        int X;
        int Y;
        GameManager gm;
        #endregion

        /// <summary>
        /// 우주선 생성자
        /// </summary>
        /// <param name="x">우주선 초기 x좌표</param>
        /// <param name="y">우주선 초기 y좌표</param>
        public Spaceship(GameManager gm, int x = 200, int y= 200)
        {
            this.gm = gm;
            if(!frmAI.isEvolving) img = Image.FromFile("spaceship.gif");
            this.x = x;
            this.y = y;
        }

        /// <summary>
        /// frmmain에서 pnGame패널이 1회 그려질 때(Paint) 호출됨.
        /// </summary>
        public void Tick()
        {
            //우주선 위치 설정
            if (mov_U) y -= speed;
            if (mov_D) y += speed;
            if (mov_R) x += speed;
            if (mov_L) x -= speed;

            //우주선 벽 충돌 체크
            if (x == 0 || x == gm.fieldSize[0] - width - 1 ||
                y == 0 || y == gm.fieldSize[1] - height - 1)
                    gm.hitMissile();    //벽 충돌이나 미사일 충돌이나 충돌은 충돌이니..
        }

        public Image getImage()
        { return img; }

    }
}
