/*********************
유전알고리즘(Genetic Algorithm) 코어 클래스
진화부분을 담당함
jk012345@gmail.com
**********************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace survivalNPC_AI
{ 
    public class GACore
    {
        const float MUTANT_DEFAULT = 0.2f;              //돌연변이(한 줄만 랜덤)
        const float SUPER_MUTANT_DEFAULT = 0.01f;       //완전돌연변이(모든 값이 랜덤)
        const float SURVIVE_DEFAULT = 0.05f;            //생존 개체 비율
        float MUTANT;
        float SURVIVE;
        //const float ELITE = 0.4f;

        int kPressure = 4;  //선택압

        /// <summary>
        /// 유전자 개체 하나의 클래스
        /// </summary>
        public class gene : IComparable
        {

            public int score;               //점수
            int thisScore;
            public int geneNo;              //유전 번호. 생성 시 가중치 문자열을 받아올 때에만 사용
            public string[,] geneString;    //geneString[인풋케이스, 인풋넘버]한 요소는 한 인풋(한 줄)
            public int fitness;         //적합도 (진화용) = (Cw-Ci)+(Cw-Cb)/(k-1), Cw > Cb, k > 1
            public gene(int fitness, int geneNo, int inputSize, int inputCase, ref string[] dbtxt)
            {
                this.score = fitness;
                this.geneNo = geneNo;

                //가중치 문자열을 받아옴. geneNo를 이용해서 부분만 받아온다.
                geneString = new string[inputCase, inputSize];
                int lineCnt = 5 + (inputSize * inputCase) * geneNo;
                for(int inputC = 0; inputC < inputCase; inputC++)
                    for(int inputnum = 0; inputnum < inputSize; inputnum++)
                        geneString[inputC, inputnum] = dbtxt[lineCnt++];
                    
            }
            public gene()
            {  }

            //IComparable 비교 연산자. 오래 살아남을수록 적합도(fitness)가 크며, 더욱 우수한 녀석으로 판정
            public int CompareTo(object g)
            {
                if (score > ((gene)g).score) return -1;
                else if (score == ((gene)g).score) return 0;
                else return 1;
            }
        };

        string filepath;
        int totalGeneration;
        int populationSize;
        int inputSize;
        int inputCase;
        int outputCase;
        int threshold;
        List<gene> Genes;
        string[] dbstr;             //유전해집합 가중치 문자열
        string referParam;          //학습지 문자열

        #region 쓰레드용 개체/변수들
        int threadNumber;
        BackgroundWorker[] simThreads; //시뮬레이션용 쓰레드
        int currentGeneNum = 0;
        int evolveLeft;    //앞으로 남은 진화 횟수
        #endregion

        Random rnd = new Random();

       /// <summary>
       /// 유전알고리즘 코어클래스 생성자
       /// </summary>
       /// <param name="txtPath"></param>
        public GACore(string dbStrTotal, int threshold, int pressure, string referParam = null, int threadNumber = 4, float mutant = MUTANT_DEFAULT, float survive = SURVIVE_DEFAULT )
        {
            MUTANT = mutant;
            SURVIVE = survive;
            kPressure = pressure;

            //변수 대입
            dbstr = dbStrTotal.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            totalGeneration = int.Parse(dbstr[0]);
            populationSize = int.Parse(dbstr[1]);
            inputSize = int.Parse(dbstr[2]);
            inputCase = int.Parse(dbstr[3]);
            outputCase = int.Parse(dbstr[4]);
            this.threshold = threshold;
            if(referParam != null) this.referParam = referParam;

            //쓰레드 할당
            this.threadNumber = threadNumber;
            simThreads = new BackgroundWorker[threadNumber];
            for (int i = 0; i < threadNumber; i++)
            {
                simThreads[i] = new BackgroundWorker();
                simThreads[i].DoWork += GACore_DoWork;
                simThreads[i].RunWorkerCompleted += GACore_RunWorkerCompleted;
            }

            //유전해 리스트 생성
            Genes = new List<gene>();
            for(int i=0; i<populationSize; i++)
                Genes.Add(new gene(0, i, inputSize, inputCase, ref dbstr));
        }

        /// <summary>
        /// 진화를 위해 유전해들을 시뮬레이션하는 함수.
        /// 비동기 쓰레드를 사용하여 시뮬레이션하고, 모든 유전해가 다 시험되면 evolveStart()함수를 호출한다
        /// </summary>
        /// <param name="evolveCnt">진화 횟수</param>
        public void evolve(int evolveCnt)
        {
            totalGeneration += 1;
            currentGeneNum = 0;
            evolveLeft = evolveCnt;
            foreach (BackgroundWorker bw in simThreads)
                bw.RunWorkerAsync();
        }

        /// <summary>
        /// 진화 함수
        /// 시뮬레이션 결과에 따라 세대를 진화시킨다
        /// </summary>
        public void evolveStart()
        {
            //유전해 정렬
            Genes.Sort();

            //새로 만들어질 신세대 해집합 생성
            List<gene> newGen = new List<gene>();

            //품질비례룰렛 선택을 위한 전체 점수 합 계산
            int totalScore = Genes.Sum(x => x.score);
            //품질비례룰렛 선택을 위한 각 유전해 적합도 계산
            int cW = totalScore - Genes.Last().score;   //biggest -> worst cost
            int cB = totalScore - Genes.First().score;  //smallest -> best cost
            for(int i=0; i<Genes.Count; i++)
            {
                int cI = totalScore - Genes[i].score;   //current cost
                Genes[i].fitness = (cW - cI) + (cW - cB) / (kPressure - 1);
            }

            //품질비례룰렛 선택을 위한 전체 적합도 합 계산
            int totalFitness = Genes.Sum(x => x.fitness);


            //----------------------진화----------------------
            //생존개체(상위 엘리트) 복사, 진화가 잘못되어도 상위 개체는 남겨서 유지
            for (int surv = 0; surv < populationSize * SURVIVE; surv++)
                newGen.Add(Genes[surv]);

            //개체수만큼 반복
            for (int newChildCnt = 0; newChildCnt < populationSize; newChildCnt++)
            {
                //반복 횟수가 개체수를 넘어가면 for 탈출
                if (newGen.Count >= populationSize) break;

                //신세대 개체 생성
                gene childGene = new gene();
                string[,] nGeneStr = new string[inputCase, inputSize];

                //일반 다음세대 유전자 생성
                if (rnd.NextDouble() > SUPER_MUTANT_DEFAULT)
                {
                    //품질비례룰렛 선택, 선택압은 kPressure
                    //두 유전자를 선택
                    int p = rnd.Next(0, totalFitness);
                    for (int f = 0, tmp = 0; f < populationSize; f++)
                    {
                        tmp += Genes[f].fitness;
                        if (tmp > p) { p = f; break; }
                    }
                    int m = rnd.Next(0, totalFitness);
                    for (int f = 0, tmp = 0; f < populationSize; f++)
                    {
                        tmp += Genes[f].fitness;
                        if (tmp > m) { m = f; break; }
                    }

                    gene papa = Genes[p];
                    gene mama = Genes[m];

                    //새로운 유전해를 전 세대의 두 유전해(papa, mama)로부터 교배하여 설정
                    for (int inputC = 0; inputC < inputCase; inputC++)
                    {

                        //균등 교차
                        for (int n = 0; n < inputSize; n++)
                        {
                            if (rnd.NextDouble() < 0.5)
                                nGeneStr[inputC, n] = papa.geneString[inputC, n];
                            else
                                nGeneStr[inputC, n] = mama.geneString[inputC, n];
                        }

                        //변이 (낮은 확률로 한 개의 유전값을 랜덤으로 재생성. 다양성 향상)
                        if (rnd.NextDouble() <= MUTANT)
                        {
                            string mutStr = "";
                            for (int ii = 0; ii < outputCase; ii++)
                                mutStr += string.Format("{0:0.0000} ", rnd.NextDouble()*2-1);
                            nGeneStr[inputC, rnd.Next(0, inputSize)] = mutStr;
                        }

                    }
                }
                //완전 돌연변이 (모든 값이 랜덤) 생성
                else
                {
                    for (int inputRange = 0; inputRange < inputSize; inputRange++)
                        for (int ic = 0; ic < inputCase ; ic++)
                            for (int oc = 0; oc < outputCase; oc++)
                                nGeneStr[ic, inputRange] += string.Format("{0:0.0000} ", rnd.NextDouble()*2-1);
                }

                childGene.geneString = nGeneStr;

                //새로운 해집합에 새로운 유전해 삽입
                newGen.Add(childGene);
            }

            //기존의 해집합을 새로운 해집합으로 대체
            Genes.Clear();
            Genes = newGen.ToList();

            //dbstr에 기록 (새로운 해집합을 가중치 문자열 변수에 기록)
            StringBuilder newStr = new StringBuilder();
            dbstr[0] = totalGeneration.ToString();

            for (int geneNum = 0; geneNum < populationSize; geneNum++)
                for (int inputcase = 0; inputcase < inputCase; inputcase++)
                    for (int inputNum = 0; inputNum < inputSize; inputNum++)
                        dbstr[5 + (geneNum * inputCase * inputSize) + (inputcase * inputSize) + inputNum] =
                            (Genes[geneNum].geneString[inputcase, inputNum]);


            //마지막 진화이면 전체 개체수 테스트 및 정렬 후 기록
            if (evolveLeft == 0 )
            {
                frmConsole.write("Total test..", true);

                //정렬을 위해 모든 개체수 다시 테스트
                evolve(0);

                //모든 개체수 테스트가 끝나면 자동으로 정렬 후 writePopulation()이 호출된다.
            }
            //마지막 진화가 아니면 evolveLeft를 1 깎고 다시 진화 실행
            else
            {
                evolveLeft -= 1;
                evolve(evolveLeft);
            }

            GC.Collect();
        }
        
        /// <summary>
        /// 시뮬레이션 쓰레드 메소드
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GACore_DoWork(object sender, DoWorkEventArgs e)
        {
            if (currentGeneNum >= Genes.Count) return;
            int ThreadGeneNum = currentGeneNum;
            currentGeneNum += 1;
            try
            {
                Simulate tmpSim = new Simulate(ref dbstr, ThreadGeneNum, threshold, referParam);
                Genes[ThreadGeneNum].score = tmpSim.Simulating();
                frmConsole.write(string.Format("[{0}] {1} -> {2}", evolveLeft, ThreadGeneNum, Genes[ThreadGeneNum].score), true);
            }
            catch { frmConsole.write("Thread error", true); }
        }

        public void writePopulation()
        {
            Genes.Sort();
            //새로운 세대 문자열을 기록
            StringBuilder newTxt = new StringBuilder();
            newTxt.AppendLine(totalGeneration.ToString());
            newTxt.AppendLine(populationSize.ToString());
            newTxt.AppendLine(inputSize.ToString());
            newTxt.AppendLine(inputCase.ToString());
            newTxt.AppendLine(outputCase.ToString());
            for (int geneNum = 0; geneNum < populationSize; geneNum++)
                for (int inputcase = 0; inputcase < inputCase; inputcase++)
                    for (int inputNum = 0; inputNum < inputSize; inputNum++)
                        newTxt.AppendLine(Genes[geneNum].geneString[inputcase, inputNum]);

            
            File.WriteAllText(frmAI.aiPath, newTxt.ToString());
            frmAI.isEvolving = false;
            frmConsole.write(frmAI.aiPath + " Evolving Finish.", true);
            frmConsole.write("Best : " + Genes[0].score, true);
        }

        /// <summary>
        /// 쓰레드 작업 완료시 재호출 또는 진화시작
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GACore_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //아직 진화를 위한 각 유전해 시뮬레이션이 안끝났다면 끝날때까지 쓰레드 돌린다.
            if(currentGeneNum < Genes.Count)
            {
                foreach(BackgroundWorker bw in simThreads)
                {
                    if(!bw.IsBusy)  //농땡이피우려는쓰레드 찾아서 일시킴
                    {
                        bw.RunWorkerAsync();
                        break;
                    }
                }
            }
            //마지막 단계까지 시뮬레이션이 완료되면
            //모든 쓰레드가 작업을 종료할때까지 대기
            //현재 작업을 완료한 쓰레드가 마지막 쓰레드라면
            //evolveStart 또는 writePopulation 호출
            else
            {
                bool isAllThreadDown = true;
                foreach (BackgroundWorker bw in simThreads)
                    if (bw.IsBusy) isAllThreadDown = false;

                if(isAllThreadDown)
                {
                    if(evolveLeft > 0)
                        evolveStart();
                    else
                        writePopulation();
                }
            }
        }
    }
}
