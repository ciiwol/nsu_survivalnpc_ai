using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace survivalNPC_AI
{
    public partial class frmAI : Form
    {
        int evolveRepeat;
        public static int OUTPUT_CASE = 4;

        #region 외부 노출용 static 변수들
        public static bool aiPlay = false;
        public static string aiPath = "";
        public static int aiThreshold = 0;
        public static bool isEvolving = false;
        #endregion

        //랜덤 객체는 정적이어야한다.
        static Random rnd = new Random();

        int cellDivCnt;
        Timer tUpdatePanel;
        GameManager mainGM;

        public frmAI(GameManager gm)
        {
            InitializeComponent();
            mainGM = gm;
        }

        private void frmAI_Load(object sender, EventArgs e)
        {
            tUpdatePanel = new Timer();
            tUpdatePanel.Interval = 10;
            tUpdatePanel.Tick += TUpdatePanel_Tick;
            tUpdatePanel.Enabled = false;
        }

        /// <summary>
        /// AI 상황판단 그래픽 패널 타이머 틱
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TUpdatePanel_Tick(object sender, EventArgs e)
        {
            if (!aiPlay) { tUpdatePanel.Enabled = false; return; }
            if (!mainGM.isGame) return;

            lblWValue.Text = string.Format("{0:0.00}",mainGM.playerShip.mov_U_Value);
            lblSValue.Text = string.Format("{0:0.00}",mainGM.playerShip.mov_D_Value);
            lblDValue.Text = string.Format("{0:0.00}",mainGM.playerShip.mov_R_Value);
            lblAValue.Text = string.Format("{0:0.00}",mainGM.playerShip.mov_L_Value);

            if (mainGM.playerShip.mov_U) lblW.BackColor = Color.Green; else lblW.BackColor = Color.White;
            if (mainGM.playerShip.mov_R) lblD.BackColor = Color.Green; else lblD.BackColor = Color.White;
            if (mainGM.playerShip.mov_D) lblS.BackColor = Color.Green; else lblS.BackColor = Color.White;
            if (mainGM.playerShip.mov_L) lblA.BackColor = Color.Green; else lblA.BackColor = Color.White;

            pnStatus.Invalidate();
        }

        //AI 상태파악 및 판단 이미지 출력
        private void pnStatus_Paint(object sender, PaintEventArgs e)
        {
            if (!aiPlay) return;
            try
            {
                int[,] aiEye = mainGM.aiSimulator.aiEye;

                Graphics g = e.Graphics;
                SolidBrush b = new SolidBrush(Color.Black);

                for (int y = 0; y < cellDivCnt; y++)
                {
                    for (int x = 0; x < cellDivCnt; x++)
                    {
                        switch (aiEye[x, y])
                        {
                            case -1:
                                b.Color = Color.Black;
                                break;
                            case 0:
                                b.Color = Color.Red;
                                break;
                            case 1:
                                b.Color = Color.Gray;
                                break;
                        }
                        //그린다.
                        g.FillRectangle(b, new Rectangle((int)((float)x * pnStatus.Width / cellDivCnt), (int)((float)y * pnStatus.Height / cellDivCnt), (int)((float)pnStatus.Width / cellDivCnt), (int)((float)pnStatus.Height / cellDivCnt)));
                    }
                }
            }
            catch
            {
                frmConsole.write("Main game status is not available.", true);
            }
        }

        /// <summary>
        /// 새로운 유전해를 만든다.
        /// 동시에 이 유전해집합이 참조하여 학습할 학습지도 만든다.
        /// 학습지는 랜덤요소가 전혀 적용되지 않는 고정된 시뮬레이션을 위한 미리보기 파일이다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNewPopulation_Click(object sender, EventArgs e)
        {
            if (isEvolving)
            {
                MessageBox.Show("Can't make new population because evolution threads are running now.");
                return;
            }

            /********************************
            경험 가중치 문자열 파일 구성은 아래와 같다
            0 : 현재 세대
            1 : 개체수
            2 : 인풋 수(범위)
            3 : 인풋 케이스(기본 2, 미사일/플레이어)
            4 : 아웃풋 케이스(기본 4* 또는 6, WASD)
            5 : 이하 유전해 값들(2번*3번 만큼 자른다)
            *********************************/
            //유전해집합 랜덤 생성
            StringBuilder newTxt = new StringBuilder();

            //해집합 생성
            newTxt.AppendLine("0"); //현재 세대 0
            newTxt.AppendLine(nNewPopSize.Value.ToString()); //개체 수
            newTxt.AppendLine(Math.Pow((int)nNewPopCellDiv.Value, 2).ToString()); //인풋 수(셀 범위)
            newTxt.AppendLine("2"); //인풋 케이스
            newTxt.AppendLine(OUTPUT_CASE.ToString()); //아웃풋 케이스
            //유전해 값들
            for (int geneNum = 0; geneNum < nNewPopSize.Value; geneNum++)
                for (int inputRange = 0; inputRange < Math.Pow((int)nNewPopCellDiv.Value, 2); inputRange++)
                    for (int inputCase = 0; inputCase < 2; inputCase++)
                    {
                        for (int outputCase = 0; outputCase < OUTPUT_CASE; outputCase++)
                        {
                            newTxt.AppendFormat("{0:0.0000} ", rnd.NextDouble()*2-1);
                        }
                        newTxt.AppendLine();
                    }

            string newPath;
            newPath = DateTime.Now.Ticks.ToString();
            File.WriteAllText(newPath, newTxt.ToString());
            newTxt.Clear();

            //학습지 생성
            int refMissileNumber = rnd.Next(8, 11);
            newTxt.Append(refMissileNumber); newTxt.Append(' ');
            for(int x=0; x<refMissileNumber; x++)
            {
                newTxt.Append(rnd.Next(1, 4));
                newTxt.Append(' ');
            }
            for (int y = 0; y < refMissileNumber; y++)
            {
                newTxt.Append(rnd.Next(1, 4));
                newTxt.Append(' ');
            }
            File.WriteAllText(newPath + "_ref", newTxt.ToString());


            //생성이 끝났으면 얘를 바로 불러오자.
            if (MessageBox.Show("Load new population?", "Hi, creator!", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                txtPopulationName.Text = newPath; 
                btnLoadPopulation.PerformClick();
            }
        }

        /// <summary>
        /// 유전해집합 파일을 불러오는 함수.
        /// 불러온다기보단 그냥 파일을 읽어서 개요정도를 표시해줌
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLoadPopulation_Click(object sender, EventArgs e)
        {
            string[] loadedPopStr = File.ReadAllLines(txtPopulationName.Text);
            lblCurrentGeneration.Text = loadedPopStr[0];
            lblPopulationSize.Text = loadedPopStr[1];
            cellDivCnt = (int)Math.Sqrt(Convert.ToInt16(loadedPopStr[2]));
            lblCellDiv.Text = cellDivCnt + " * " + cellDivCnt;
            aiPath = txtPopulationName.Text;
            var th = 0;// cellDivCnt * cellDivCnt * 0.5;
            nThreshold.Value = (int)th;
        }

        /// <summary>
        /// 진화 버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEvolve_Click(object sender, EventArgs e)
        {
            btnLoadPopulation.PerformClick();

            //이미 진화중이면 진화작업중임을 알린다.
            if(isEvolving)
            {
                MessageBox.Show("Evolution thread is already running now");
                return;
            }

            //진화 플래그 true
            isEvolving = true;

            //총 진화 횟수 넣기
            evolveRepeat = (int)nEvolveRepeat.Value;

            //진화
            //먼저 읽는다.
            if (!File.Exists(txtPopulationName.Text)) { System.Windows.Forms.MessageBox.Show("POPULATION FILE NOT EXIST"); return; }
            if (!File.Exists(txtPopulationName.Text + "_ref")) { System.Windows.Forms.MessageBox.Show("REFERENCE FILE NOT EXIST"); return; }
            string filepath = txtPopulationName.Text;

            string dbStrTotal = File.ReadAllText(filepath);
            string referParam = File.ReadAllText(filepath + "_ref");

            //진화 인스턴스 생성 및 진화 실행
            GACore gaCore = new GACore(dbStrTotal, (int)nThreshold.Value, (int)nSelPressure.Value, referParam, (int)nThreadNumber.Value, (float)nEvolveMutant.Value, (float)nEvolveSurvive.Value);
            gaCore.evolve(evolveRepeat);

        }

        //현재 해집합의 최상위 유전자를 가지고 빠른 시뮬레이션 테스트 실시
        private void btnTestReference_Click(object sender, EventArgs e)
        {
            //테스트용 시뮬레이션 인스턴스 생성
            Simulate fastSim = new Simulate(new GameManager(), txtPopulationName.Text, 0, (int)nThreshold.Value);
            int simResult = fastSim.Simulating();
            MessageBox.Show(string.Format("{0} tick", simResult), "Simulation result");
        }

        private void btnOpenDirectory_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("explorer.exe", Application.StartupPath);
        }

        private void chkAIPlay_CheckedChanged(object sender, EventArgs e)
        {
            if(chkAIPlay.Checked)
            {
                aiThreshold = (int)nThreshold.Value;
                aiPath = txtPopulationName.Text;
                aiPlay = chkAIPlay.Checked;
                tUpdatePanel.Enabled = true;
            }
            else
            {
                aiPlay = chkAIPlay.Checked;
            }
        }
    }
}
