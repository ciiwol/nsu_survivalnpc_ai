﻿namespace survivalNPC_AI
{
    partial class frmAI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblD = new System.Windows.Forms.Label();
            this.lblA = new System.Windows.Forms.Label();
            this.lblS = new System.Windows.Forms.Label();
            this.lblW = new System.Windows.Forms.Label();
            this.chkAIPlay = new System.Windows.Forms.CheckBox();
            this.txtPopulationName = new System.Windows.Forms.TextBox();
            this.btnLoadPopulation = new System.Windows.Forms.Button();
            this.btnNewPopulation = new System.Windows.Forms.Button();
            this.btnEvolve = new System.Windows.Forms.Button();
            this.nEvolveRepeat = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.nThreshold = new System.Windows.Forms.NumericUpDown();
            this.nNewPopSize = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.nNewPopCellDiv = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.nSelPressure = new System.Windows.Forms.NumericUpDown();
            this.nThreadNumber = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.nEvolveMutant = new System.Windows.Forms.NumericUpDown();
            this.nEvolveSurvive = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.btnOpenDirectory = new System.Windows.Forms.Button();
            this.lblCellDiv = new System.Windows.Forms.Label();
            this.lblPopulationSize = new System.Windows.Forms.Label();
            this.lblCurrentGeneration = new System.Windows.Forms.Label();
            this.btnTestReference = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.pnStatus = new survivalNPC_AI.pnGame();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lblWValue = new System.Windows.Forms.Label();
            this.lblSValue = new System.Windows.Forms.Label();
            this.lblAValue = new System.Windows.Forms.Label();
            this.lblDValue = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nEvolveRepeat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nThreshold)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nNewPopSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nNewPopCellDiv)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nSelPressure)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nThreadNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nEvolveMutant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nEvolveSurvive)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblD
            // 
            this.lblD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblD.Location = new System.Drawing.Point(201, 201);
            this.lblD.Name = "lblD";
            this.lblD.Size = new System.Drawing.Size(20, 20);
            this.lblD.TabIndex = 12;
            this.lblD.Text = "D";
            this.lblD.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblA
            // 
            this.lblA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblA.Location = new System.Drawing.Point(159, 201);
            this.lblA.Name = "lblA";
            this.lblA.Size = new System.Drawing.Size(20, 20);
            this.lblA.TabIndex = 13;
            this.lblA.Text = "A";
            this.lblA.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblS
            // 
            this.lblS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblS.Location = new System.Drawing.Point(180, 201);
            this.lblS.Name = "lblS";
            this.lblS.Size = new System.Drawing.Size(20, 20);
            this.lblS.TabIndex = 14;
            this.lblS.Text = "S";
            this.lblS.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblW
            // 
            this.lblW.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblW.Location = new System.Drawing.Point(180, 180);
            this.lblW.Name = "lblW";
            this.lblW.Size = new System.Drawing.Size(20, 20);
            this.lblW.TabIndex = 15;
            this.lblW.Text = "W";
            this.lblW.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chkAIPlay
            // 
            this.chkAIPlay.AutoSize = true;
            this.chkAIPlay.Location = new System.Drawing.Point(6, 51);
            this.chkAIPlay.Name = "chkAIPlay";
            this.chkAIPlay.Size = new System.Drawing.Size(63, 16);
            this.chkAIPlay.TabIndex = 11;
            this.chkAIPlay.Text = "AI play";
            this.chkAIPlay.UseVisualStyleBackColor = true;
            this.chkAIPlay.CheckedChanged += new System.EventHandler(this.chkAIPlay_CheckedChanged);
            // 
            // txtPopulationName
            // 
            this.txtPopulationName.BackColor = System.Drawing.Color.White;
            this.txtPopulationName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPopulationName.ForeColor = System.Drawing.Color.Black;
            this.txtPopulationName.Location = new System.Drawing.Point(6, 22);
            this.txtPopulationName.Name = "txtPopulationName";
            this.txtPopulationName.Size = new System.Drawing.Size(218, 21);
            this.txtPopulationName.TabIndex = 17;
            // 
            // btnLoadPopulation
            // 
            this.btnLoadPopulation.Location = new System.Drawing.Point(166, 49);
            this.btnLoadPopulation.Name = "btnLoadPopulation";
            this.btnLoadPopulation.Size = new System.Drawing.Size(58, 23);
            this.btnLoadPopulation.TabIndex = 18;
            this.btnLoadPopulation.Text = "Load";
            this.btnLoadPopulation.UseVisualStyleBackColor = true;
            this.btnLoadPopulation.Click += new System.EventHandler(this.btnLoadPopulation_Click);
            // 
            // btnNewPopulation
            // 
            this.btnNewPopulation.Location = new System.Drawing.Point(144, 20);
            this.btnNewPopulation.Name = "btnNewPopulation";
            this.btnNewPopulation.Size = new System.Drawing.Size(75, 50);
            this.btnNewPopulation.TabIndex = 19;
            this.btnNewPopulation.Text = "New Pop.";
            this.btnNewPopulation.UseVisualStyleBackColor = true;
            this.btnNewPopulation.Click += new System.EventHandler(this.btnNewPopulation_Click);
            // 
            // btnEvolve
            // 
            this.btnEvolve.Location = new System.Drawing.Point(4, 211);
            this.btnEvolve.Name = "btnEvolve";
            this.btnEvolve.Size = new System.Drawing.Size(75, 23);
            this.btnEvolve.TabIndex = 20;
            this.btnEvolve.Text = "Evolve";
            this.btnEvolve.UseVisualStyleBackColor = true;
            this.btnEvolve.Click += new System.EventHandler(this.btnEvolve_Click);
            // 
            // nEvolveRepeat
            // 
            this.nEvolveRepeat.Location = new System.Drawing.Point(86, 211);
            this.nEvolveRepeat.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nEvolveRepeat.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nEvolveRepeat.Name = "nEvolveRepeat";
            this.nEvolveRepeat.Size = new System.Drawing.Size(40, 21);
            this.nEvolveRepeat.TabIndex = 21;
            this.nEvolveRepeat.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Enabled = false;
            this.label1.Location = new System.Drawing.Point(5, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 12);
            this.label1.TabIndex = 22;
            this.label1.Text = "Threshold value :";
            // 
            // nThreshold
            // 
            this.nThreshold.DecimalPlaces = 2;
            this.nThreshold.Enabled = false;
            this.nThreshold.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.nThreshold.Location = new System.Drawing.Point(115, 25);
            this.nThreshold.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nThreshold.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.nThreshold.Name = "nThreshold";
            this.nThreshold.Size = new System.Drawing.Size(67, 21);
            this.nThreshold.TabIndex = 23;
            // 
            // nNewPopSize
            // 
            this.nNewPopSize.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nNewPopSize.Location = new System.Drawing.Point(102, 23);
            this.nNewPopSize.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nNewPopSize.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nNewPopSize.Name = "nNewPopSize";
            this.nNewPopSize.Size = new System.Drawing.Size(36, 21);
            this.nNewPopSize.TabIndex = 24;
            this.nNewPopSize.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 12);
            this.label2.TabIndex = 25;
            this.label2.Text = "Population size";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 12);
            this.label3.TabIndex = 26;
            this.label3.Text = "Cell division cnt";
            // 
            // nNewPopCellDiv
            // 
            this.nNewPopCellDiv.Location = new System.Drawing.Point(102, 49);
            this.nNewPopCellDiv.Maximum = new decimal(new int[] {
            400,
            0,
            0,
            0});
            this.nNewPopCellDiv.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nNewPopCellDiv.Name = "nNewPopCellDiv";
            this.nNewPopCellDiv.Size = new System.Drawing.Size(36, 21);
            this.nNewPopCellDiv.TabIndex = 24;
            this.nNewPopCellDiv.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnNewPopulation);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.nNewPopSize);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.nNewPopCellDiv);
            this.groupBox1.Location = new System.Drawing.Point(13, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(228, 82);
            this.groupBox1.TabIndex = 27;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Generate genes";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.nSelPressure);
            this.groupBox2.Controls.Add(this.nThreadNumber);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.nEvolveMutant);
            this.groupBox2.Controls.Add(this.nEvolveSurvive);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.btnOpenDirectory);
            this.groupBox2.Controls.Add(this.lblCellDiv);
            this.groupBox2.Controls.Add(this.lblPopulationSize);
            this.groupBox2.Controls.Add(this.lblCurrentGeneration);
            this.groupBox2.Controls.Add(this.btnTestReference);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtPopulationName);
            this.groupBox2.Controls.Add(this.btnLoadPopulation);
            this.groupBox2.Controls.Add(this.btnEvolve);
            this.groupBox2.Controls.Add(this.nEvolveRepeat);
            this.groupBox2.Location = new System.Drawing.Point(12, 100);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(229, 243);
            this.groupBox2.TabIndex = 28;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Load genes";
            // 
            // nSelPressure
            // 
            this.nSelPressure.BackColor = System.Drawing.Color.White;
            this.nSelPressure.Location = new System.Drawing.Point(70, 144);
            this.nSelPressure.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.nSelPressure.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nSelPressure.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nSelPressure.Name = "nSelPressure";
            this.nSelPressure.Size = new System.Drawing.Size(48, 21);
            this.nSelPressure.TabIndex = 32;
            this.nSelPressure.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // nThreadNumber
            // 
            this.nThreadNumber.Location = new System.Drawing.Point(193, 184);
            this.nThreadNumber.Maximum = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.nThreadNumber.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nThreadNumber.Name = "nThreadNumber";
            this.nThreadNumber.Size = new System.Drawing.Size(31, 21);
            this.nThreadNumber.TabIndex = 31;
            this.nThreadNumber.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(172, 166);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(52, 12);
            this.label10.TabIndex = 30;
            this.label10.Text = "Threads";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 188);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(43, 12);
            this.label9.TabIndex = 29;
            this.label9.Text = "Mutant";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(8, 142);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(56, 12);
            this.label11.TabIndex = 29;
            this.label11.Text = "Pressure";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 167);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 12);
            this.label8.TabIndex = 29;
            this.label8.Text = "Survive";
            // 
            // nEvolveMutant
            // 
            this.nEvolveMutant.DecimalPlaces = 2;
            this.nEvolveMutant.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.nEvolveMutant.Location = new System.Drawing.Point(70, 187);
            this.nEvolveMutant.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nEvolveMutant.Name = "nEvolveMutant";
            this.nEvolveMutant.Size = new System.Drawing.Size(48, 21);
            this.nEvolveMutant.TabIndex = 28;
            this.nEvolveMutant.Value = new decimal(new int[] {
            15,
            0,
            0,
            131072});
            // 
            // nEvolveSurvive
            // 
            this.nEvolveSurvive.DecimalPlaces = 2;
            this.nEvolveSurvive.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.nEvolveSurvive.Location = new System.Drawing.Point(70, 165);
            this.nEvolveSurvive.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nEvolveSurvive.Name = "nEvolveSurvive";
            this.nEvolveSurvive.Size = new System.Drawing.Size(48, 21);
            this.nEvolveSurvive.TabIndex = 28;
            this.nEvolveSurvive.Value = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(132, 216);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 12);
            this.label7.TabIndex = 27;
            this.label7.Text = "times";
            // 
            // btnOpenDirectory
            // 
            this.btnOpenDirectory.Location = new System.Drawing.Point(193, 79);
            this.btnOpenDirectory.Name = "btnOpenDirectory";
            this.btnOpenDirectory.Size = new System.Drawing.Size(31, 23);
            this.btnOpenDirectory.TabIndex = 26;
            this.btnOpenDirectory.Text = "...";
            this.btnOpenDirectory.UseVisualStyleBackColor = true;
            this.btnOpenDirectory.Click += new System.EventHandler(this.btnOpenDirectory_Click);
            // 
            // lblCellDiv
            // 
            this.lblCellDiv.AutoSize = true;
            this.lblCellDiv.Location = new System.Drawing.Point(86, 87);
            this.lblCellDiv.Name = "lblCellDiv";
            this.lblCellDiv.Size = new System.Drawing.Size(11, 12);
            this.lblCellDiv.TabIndex = 25;
            this.lblCellDiv.Text = "0";
            // 
            // lblPopulationSize
            // 
            this.lblPopulationSize.AutoSize = true;
            this.lblPopulationSize.Location = new System.Drawing.Point(86, 70);
            this.lblPopulationSize.Name = "lblPopulationSize";
            this.lblPopulationSize.Size = new System.Drawing.Size(11, 12);
            this.lblPopulationSize.TabIndex = 25;
            this.lblPopulationSize.Text = "0";
            // 
            // lblCurrentGeneration
            // 
            this.lblCurrentGeneration.AutoSize = true;
            this.lblCurrentGeneration.Location = new System.Drawing.Point(86, 53);
            this.lblCurrentGeneration.Name = "lblCurrentGeneration";
            this.lblCurrentGeneration.Size = new System.Drawing.Size(11, 12);
            this.lblCurrentGeneration.TabIndex = 25;
            this.lblCurrentGeneration.Text = "0";
            // 
            // btnTestReference
            // 
            this.btnTestReference.Enabled = false;
            this.btnTestReference.Location = new System.Drawing.Point(166, 108);
            this.btnTestReference.Name = "btnTestReference";
            this.btnTestReference.Size = new System.Drawing.Size(58, 23);
            this.btnTestReference.TabIndex = 24;
            this.btnTestReference.Text = "test";
            this.btnTestReference.UseVisualStyleBackColor = true;
            this.btnTestReference.Click += new System.EventHandler(this.btnTestReference_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 87);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 12);
            this.label6.TabIndex = 23;
            this.label6.Text = "Cell div cnt";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 70);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 12);
            this.label5.TabIndex = 23;
            this.label5.Text = "Pop Size";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 12);
            this.label4.TabIndex = 22;
            this.label4.Text = "Current Gen";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lblDValue);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.lblAValue);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.lblSValue);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.lblWValue);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.pnStatus);
            this.groupBox3.Controls.Add(this.lblW);
            this.groupBox3.Controls.Add(this.nThreshold);
            this.groupBox3.Controls.Add(this.lblS);
            this.groupBox3.Controls.Add(this.lblA);
            this.groupBox3.Controls.Add(this.lblD);
            this.groupBox3.Controls.Add(this.chkAIPlay);
            this.groupBox3.Location = new System.Drawing.Point(13, 349);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(228, 228);
            this.groupBox3.TabIndex = 29;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "AI Monitor";
            // 
            // pnStatus
            // 
            this.pnStatus.BackColor = System.Drawing.Color.Black;
            this.pnStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnStatus.Location = new System.Drawing.Point(5, 71);
            this.pnStatus.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.pnStatus.Name = "pnStatus";
            this.pnStatus.Size = new System.Drawing.Size(150, 150);
            this.pnStatus.TabIndex = 10;
            this.pnStatus.Paint += new System.Windows.Forms.PaintEventHandler(this.pnStatus_Paint);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(163, 71);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(15, 12);
            this.label12.TabIndex = 24;
            this.label12.Text = "W";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(163, 88);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(13, 12);
            this.label13.TabIndex = 24;
            this.label13.Text = "S";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(163, 105);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(13, 12);
            this.label14.TabIndex = 24;
            this.label14.Text = "A";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(163, 122);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(13, 12);
            this.label15.TabIndex = 24;
            this.label15.Text = "D";
            // 
            // lblWValue
            // 
            this.lblWValue.AutoSize = true;
            this.lblWValue.Location = new System.Drawing.Point(178, 71);
            this.lblWValue.Name = "lblWValue";
            this.lblWValue.Size = new System.Drawing.Size(15, 12);
            this.lblWValue.TabIndex = 24;
            this.lblWValue.Text = "w";
            // 
            // lblSValue
            // 
            this.lblSValue.AutoSize = true;
            this.lblSValue.Location = new System.Drawing.Point(178, 88);
            this.lblSValue.Name = "lblSValue";
            this.lblSValue.Size = new System.Drawing.Size(12, 12);
            this.lblSValue.TabIndex = 24;
            this.lblSValue.Text = "s";
            // 
            // lblAValue
            // 
            this.lblAValue.AutoSize = true;
            this.lblAValue.Location = new System.Drawing.Point(178, 105);
            this.lblAValue.Name = "lblAValue";
            this.lblAValue.Size = new System.Drawing.Size(12, 12);
            this.lblAValue.TabIndex = 24;
            this.lblAValue.Text = "a";
            // 
            // lblDValue
            // 
            this.lblDValue.AutoSize = true;
            this.lblDValue.Location = new System.Drawing.Point(178, 122);
            this.lblDValue.Name = "lblDValue";
            this.lblDValue.Size = new System.Drawing.Size(12, 12);
            this.lblDValue.TabIndex = 24;
            this.lblDValue.Text = "d";
            // 
            // frmAI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(253, 586);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmAI";
            this.Text = "GA AI Control Center";
            this.Load += new System.EventHandler(this.frmAI_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nEvolveRepeat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nThreshold)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nNewPopSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nNewPopCellDiv)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nSelPressure)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nThreadNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nEvolveMutant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nEvolveSurvive)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblD;
        private System.Windows.Forms.Label lblA;
        private System.Windows.Forms.Label lblS;
        private System.Windows.Forms.Label lblW;
        private System.Windows.Forms.CheckBox chkAIPlay;
        private pnGame pnStatus;
        private System.Windows.Forms.TextBox txtPopulationName;
        private System.Windows.Forms.Button btnLoadPopulation;
        private System.Windows.Forms.Button btnNewPopulation;
        private System.Windows.Forms.Button btnEvolve;
        private System.Windows.Forms.NumericUpDown nEvolveRepeat;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown nThreshold;
        private System.Windows.Forms.NumericUpDown nNewPopSize;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown nNewPopCellDiv;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnTestReference;
        private System.Windows.Forms.Label lblCellDiv;
        private System.Windows.Forms.Label lblPopulationSize;
        private System.Windows.Forms.Label lblCurrentGeneration;
        private System.Windows.Forms.Button btnOpenDirectory;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown nEvolveMutant;
        private System.Windows.Forms.NumericUpDown nEvolveSurvive;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown nThreadNumber;
        private System.Windows.Forms.NumericUpDown nSelPressure;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblDValue;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lblAValue;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lblSValue;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lblWValue;
        private System.Windows.Forms.Label label12;
    }
}