﻿/********************************
게임 시뮬레이션 클래스
경험 가중치 문자열 파일 구성은 아래와 같다
0 : 현재 세대
1 : 개체수
2 : 인풋 수
3 : 인풋 케이스(기본 2, 미사일/벽)
4 : 아웃풋 케이스(기본 6, WSW, WSM, WSS, ADA, ADM, ADD -> 상, 중, 하 / 좌, 중, 우)
4* : 아웃풋 케이스(기본 4, WASD) 
5 : 이하 유전해 값들(2번*3번 만큼 자른다)
jk012345@gmail.com
*********************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace survivalNPC_AI
{
    public class Simulate
    {
        const int MISSILE = 0, WALL = 1, NONE = -1;    //필드의 개체에 번호 부여
        const bool CONSOLE_DEBUG = false;           //테스트용
        const int CELL_SIZE = 10;

        #region public 속성
        public int[,] aiEye;
        public bool isGame = false; //현재 시뮬레이션이 진행중인지
        public bool isInReal = false;   //실제 게임에 적용중인지
        #endregion

        #region private 속성

        int EVOLVE_TEST_MAX_TICK = 5000;                    //학습시 최대 틱 제한

        bool useReference;                           //true면 학습지(기준 필드) 이용

        string[] dbstr;             //불러온 유전해집합TXT
        int totalGeneration;        //현재 시뮬레이션하는 유전해의 세대
        int populationAmount;       //해집합 크기(개체수)
        int inputAmount;            //한 입력 범주당 입력값 개수
        int inputCaseCnt;           //입력 범주 개수
        int outputCaseCnt;          //출력 범주 개수
        int cellDiv;              //탐지 셀 범위
        int threshold;              //역치값. 시간이 허락한다면 얘도 유전알고리즘으로 설정되게 하고싶음


        int tickCnt = 0;            //현재 시뮬레이션의 진행 Tick
        int crash = 0;              //현재 시뮬레이션의 충돌 횟수

        int curGeneNum = 0;         //현재 시뮬레이션중인 유전해 번호

        Random rnd = new Random();

        GameManager gm;
        #endregion

        /// <summary>
        /// 외부 가중치 txt파일을 불러와서 실제 AI플레이에 사용할 때 호출되는 생성자
        /// GameManager를 따로 만들지 않고 메인 게임의 GameManager를 사용
        /// </summary>
        /// <param name="txtPath">가중치txt 파일이름</param>
        /// <param name="geneNo">유전해 번호</param>
        /// <param name="threshold">역치값</param>
        public Simulate(GameManager gm, string txtPath, int geneNo, int threshold)
        {
            this.gm = gm;
            isInReal = true;
            //gm이 설정된상태 -> 실제 게임이 진행중 ->
            //이미 게임매니저 Tick이 실제로 돌아가는 상태 -> gm.Tick을 따로 호출할 필요 X

            if(!File.Exists(txtPath)) { MessageBox.Show("FILE IS NOT EXIST"); return; }
            //외부 txt파일을 내부변수 dbStr에 저장
            //학습지 txt파일이 지정된 경우에는 학습지 txt파일도 standardField에 저장
            dbstr = File.ReadAllLines(txtPath);

            this.threshold = threshold;
            simulate_new(geneNo);
        }

        /// <summary>
        /// 내부 가중치 문자열을 이용해서 진화할 때 호출되는 생성자
        /// 자체적으로 GameManager를 생성하여 시뮬레이션
        /// </summary>
        /// <param name="dbtxt"></param>
        /// <param name="geneNo"></param>
        /// <param name="standardField"></param>
        public Simulate(ref string[] dbtxt, int geneNo, int threshold, string referParam = null)
        {
            //내부 가중치 string변수를 dbStr에 저장
            dbstr =  dbtxt.ToArray();
            this.threshold = threshold;

            //게임매니저 생성
            gm = new GameManager();
            //학습지 불러오기
            int mNumber = 0;
            int[] mvX = null, mvY = null;
            if(referParam != null)
            {
                string[] fParams = referParam.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                mNumber = Convert.ToInt16(fParams[0]);
                mvX = new int[mNumber]; mvY = new int[mNumber];
                for(int i=0; i<mNumber; i++)
                {
                    mvX[i] = Convert.ToInt16(fParams[i+1]);
                    mvY[i] = Convert.ToInt16(fParams[i + 1 + mNumber]);
                }
            }

            //게임매니저 초기화
            gm.Init(true, false, mNumber, mvX, mvY);

            //플레이어 초기 위치는 200, 200
            gm.playerShip.x = 200;
            gm.playerShip.y = 200;

            simulate_new(geneNo);
        }


        /// <summary>
        /// 새로운 시뮬레이션 생성
        /// </summary>
        /// <param name="geneNo">유전자 번호</param>
        void simulate_new(int geneNo)
        {
            //값 불러옴
            totalGeneration = int.Parse(dbstr[0]);
            populationAmount = int.Parse(dbstr[1]); 
            inputAmount = int.Parse(dbstr[2]); 
            inputCaseCnt = int.Parse(dbstr[3]); 
            outputCaseCnt = int.Parse(dbstr[4]);
            cellDiv = (int)Math.Sqrt(inputAmount);
            curGeneNum = geneNo;
            aiEye = new int[cellDiv, cellDiv];

        }

        //시뮬레이션
        public int Simulating()
        {
            isGame = true;
            while(isGame)
            {
                simulateTick(); //내부 연산일 경우 그냥 연산 반복.
                if (tickCnt >= EVOLVE_TEST_MAX_TICK) break; //학습지 틱 제한을 넘어가면 종료
            }
            return tickCnt;
        }

        /// <summary>
        /// 시뮬레이션 자체적으로 사용되는 틱 함수.
        /// GameManager의 Tick과 같은 역할이지만 사용하는 곳이 다르다.
        /// </summary>
        public void simulateTick()
        {
            //현재 상황을 파악하고 어디로 이동할지 결정함
            float[] moveKey = judgeSituation();

            //이동함
            playerMove(moveKey);

            //gm에서 1틱 진행
            if (!isInReal) gm.Tick();
            //isInReal이 true일 땐 gm.Tick을 호출할 필요가 없다. (이미 메인 gm이 돌아가는중이기 때문)
            //고로 테스트중이거나 ai용 시뮬레이션 클래스로 작동중일땐 
            //gm.tick이 실행되지 않으므로 무한루프에 빠지지 않는다.

            //게임오버됐는지?
            isGame = gm.isGame;

            //진행시간 1 추가
            tickCnt += 1;
        }

        /// <summary>
        /// 이동 키를 결정하는 함수
        /// </summary>
        /// <returns>int 방향별 자극값 배열(0U, 1R, 2D, 3L)</returns>
        float[] judgeSituation()
        {
            //필드에서 플레이어의 주변을 받아온다. (탐지 범위만큼)

            #region AI 인식 필드 갱신 (aiEye 갱신)
            #region old
            //for(int y=0; y< cellDiv; y++)
            //   for(int x=0; x< cellDiv; x++)
            //   {
            //       //실제 조사될 필드의 X, Y좌표
            //       int X = x * gm.fieldSize[0] / cellDiv;
            //       int Y = y * gm.fieldSize[1] / cellDiv;

            //       //일단 빈공간으로 채운다
            //       aiEye[x, y] = NONE;

            //       //게임영역 밖이면 벽으로 채운다. (하지만 아래 코드는 실행되지 않는다. aiEye에 벽은 없을거기 때문)
            //       //if (X < 0 || X >= gm.fieldSize[0])
            //       //{ aiEye[x, y] = PLAYER; continue; }
            //       //if (Y < 0 || Y >= gm.fieldSize[1])
            //       //{ aiEye[x, y] = PLAYER; continue; }

            //       //gm.field[X, Y]의 값을 aiEye[x, y]에 넣는다.
            //       for(int fy=0; fy<gm.fieldSize[1]/cellDiv; fy++)
            //           for(int fx=0; fx< gm.fieldSize[0]/cellDiv; fx++)
            //               if(gm.field[X + fx, Y + fy] != NONE)
            //                   aiEye[x, y] = gm.field[X+fx, Y+fy];
            //   }
            #endregion

            //현재 플레이어의 x, y 중앙좌표
            int midX = gm.playerShip.x;// + gm.playerShip.width / 2;
            int midY = gm.playerShip.y;// + gm.playerShip.height / 2;
            for (int y = 0; y < cellDiv; y++)
                for (int x = 0; x < cellDiv; x++)
                {
                    //실제 조사될 필드의 X, Y좌표 (0~400) -> 플레이어의 중앙으로부터 반경 cellDiv만큼의 정사각형을 cellDiv^2만큼 순차적으로 돈다.
                    int X = midX + ((x - cellDiv / 2) * CELL_SIZE); //셀의 크기는 한칸에 10이라고 정의
                    int Y = midY + ((y - cellDiv / 2) * CELL_SIZE);

                    //aiEye 초기화
                    for (int cx = 0; cx < CELL_SIZE; cx++)
                        for (int cy = 0; cy < CELL_SIZE; cy++)
                            aiEye[x, y] = NONE;

                    //게임영역 밖이면 벽으로 채운다.
                    if (X < 0 || X+CELL_SIZE >= gm.fieldSize[0])
                    { aiEye[x, y] = WALL; continue; }
                    if (Y < 0 || Y+CELL_SIZE >= gm.fieldSize[1])
                    { aiEye[x, y] = WALL; continue; }

                    //gm.field[X, Y]의 값을 aiEye[x, y]에 넣는다.
                    //X, Y에서 CELL_SIZE 안에 무언가(미사일) 있으면 그 무언가(미사일)를 넣어요.
                    //이미 aiEye[x, y]에 뭔가 있으면 덮어씌우지 않아요.
                    for (int cx = 0; cx < CELL_SIZE; cx++)
                        for(int cy = 0; cy < CELL_SIZE; cy++)
                        if (aiEye[x, y] == NONE)
                            aiEye[x, y] = gm.field[X+cx, Y+cy];
                }
            #endregion

            //현재 유전해(curGeneNum)의 가중치 값(문자열) 가져오기
            int totalOneGeneLines = int.Parse(dbstr[2]) * int.Parse(dbstr[3]);
            int startline = 5 + totalOneGeneLines * curGeneNum;
            int endline = startline + totalOneGeneLines;

            //현재 유전해의 전체 가중치 값을 curGeneTxt에 복사 (다루기 쉽게)
            string[] curGeneTxt = new string[totalOneGeneLines];    //curGeneTxt : 현재 유전해 가중치테이블 문자열의 배열
            for (int i = 0; i < totalOneGeneLines; i++)
                curGeneTxt[i] = dbstr[startline + i];

            //플레이어 주변 값(nearPlayer)으로부터 가중치 계산
            float[] outputs = new float[outputCaseCnt]; //outputCaseCnt = 4
            for (int i = 0; i < outputCaseCnt; i++)
                outputs[i] = 0; //outputs 0으로 초기화
            
            for(int i = 0; i<inputAmount; i++)
            {
                //한 입력값에 대한 출력을 결정하기 위해 가중치에서 한 줄(한 입력값) 불러옴
                //"총인풋개수 * 현재인풋 + 좌표" 대충 이렇게되려나. 현재인풋이 0이면 '좌표'만 가져오고
                //현재 인풋이 1이면 '총인풋개수+좌표'의 값을 가져온다.
                int curInput = inputAmount * aiEye[i / cellDiv, i % cellDiv] + i;
                if (curInput < 0) continue;
                string[] curInputData = curGeneTxt[curInput].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);       //한 인풋 데이터(한 줄)

                //각 아웃풋에 가중치 넣기
                for(int output=0; output < outputCaseCnt; output++)
                {
                    float addVal = float.Parse(curInputData[output]);
                    outputs[output] += addVal;
                }
            }

            return outputs;  //계산된 결과 출력
        }

        //좌 or 우로 이동
        void playerMove(float[] moveKey)
        {
            gm.playerShip.mov_U = false;
            gm.playerShip.mov_R = false;
            gm.playerShip.mov_D = false;
            gm.playerShip.mov_L = false;

            gm.playerShip.mov_U_Value = moveKey[0];
            gm.playerShip.mov_R_Value = moveKey[1];
            gm.playerShip.mov_D_Value = moveKey[2];
            gm.playerShip.mov_L_Value = moveKey[3];

            //if (outputCaseCnt == 6)
            //{

            //    //0, 1, 2는 상/중/하
            //    if (moveKey[0] > moveKey[1] && moveKey[0] > moveKey[2])
            //        gm.playerShip.mov_U = true;
            //    if (moveKey[2] > moveKey[1] && moveKey[2] > moveKey[0])
            //        gm.playerShip.mov_D = true;

            //    //3, 4, 5는 좌/중/우
            //    if (moveKey[3] > moveKey[4] && moveKey[3] > moveKey[5])
            //        gm.playerShip.mov_L = true;
            //    if (moveKey[5] > moveKey[4] && moveKey[5] > moveKey[3])
            //        gm.playerShip.mov_R = true;
            //}
            //else
            {
                gm.playerShip.mov_U = moveKey[0] > threshold;
                gm.playerShip.mov_R = moveKey[1] > threshold;
                gm.playerShip.mov_D = moveKey[2] > threshold;
                gm.playerShip.mov_L = moveKey[3] > threshold;
            }
        }


    }
}
