﻿namespace survivalNPC_AI
{
    partial class frmmain
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnStart = new System.Windows.Forms.Button();
            this.timUpdate = new System.Windows.Forms.Timer(this.components);
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.pnGameField = new survivalNPC_AI.pnGame();
            this.chkUseRef = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.nMissileCnt = new System.Windows.Forms.NumericUpDown();
            this.chkMissileCntRand = new System.Windows.Forms.CheckBox();
            this.chkAutoRestart = new System.Windows.Forms.CheckBox();
            this.chkInfiniteMode = new System.Windows.Forms.CheckBox();
            this.pnCrashProgress_Total = new System.Windows.Forms.Panel();
            this.pnProgrssCrash_Guage = new System.Windows.Forms.Panel();
            this.lblCrashTotalCnt = new System.Windows.Forms.Label();
            this.lblCrashRate = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnStop = new System.Windows.Forms.Button();
            this.lblScore = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.mnu = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.끝내기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aISettingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consoleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nMissileCnt)).BeginInit();
            this.pnCrashProgress_Total.SuspendLayout();
            this.mnu.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(9, 368);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(188, 31);
            this.btnStart.TabIndex = 0;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.start_Click);
            // 
            // timUpdate
            // 
            this.timUpdate.Interval = 10;
            this.timUpdate.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 40);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.Color.White;
            this.splitContainer1.Panel1.Controls.Add(this.pnGameField);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.chkUseRef);
            this.splitContainer1.Panel2.Controls.Add(this.label3);
            this.splitContainer1.Panel2.Controls.Add(this.nMissileCnt);
            this.splitContainer1.Panel2.Controls.Add(this.chkMissileCntRand);
            this.splitContainer1.Panel2.Controls.Add(this.chkAutoRestart);
            this.splitContainer1.Panel2.Controls.Add(this.chkInfiniteMode);
            this.splitContainer1.Panel2.Controls.Add(this.pnCrashProgress_Total);
            this.splitContainer1.Panel2.Controls.Add(this.lblCrashTotalCnt);
            this.splitContainer1.Panel2.Controls.Add(this.lblCrashRate);
            this.splitContainer1.Panel2.Controls.Add(this.label4);
            this.splitContainer1.Panel2.Controls.Add(this.label2);
            this.splitContainer1.Panel2.Controls.Add(this.btnStop);
            this.splitContainer1.Panel2.Controls.Add(this.btnStart);
            this.splitContainer1.Panel2.Controls.Add(this.lblScore);
            this.splitContainer1.Panel2.Controls.Add(this.label1);
            this.splitContainer1.Panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.splitContainer1_Panel2_Paint);
            this.splitContainer1.Size = new System.Drawing.Size(739, 392);
            this.splitContainer1.SplitterDistance = 408;
            this.splitContainer1.TabIndex = 7;
            // 
            // pnGameField
            // 
            this.pnGameField.BackColor = System.Drawing.Color.Black;
            this.pnGameField.Location = new System.Drawing.Point(4, 4);
            this.pnGameField.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.pnGameField.Name = "pnGameField";
            this.pnGameField.Size = new System.Drawing.Size(400, 400);
            this.pnGameField.TabIndex = 1;
            this.pnGameField.Paint += new System.Windows.Forms.PaintEventHandler(this.pnGame1_Paint);
            this.pnGameField.KeyDown += new System.Windows.Forms.KeyEventHandler(this.pnGame1_KeyDown);
            this.pnGameField.KeyUp += new System.Windows.Forms.KeyEventHandler(this.pnGame1_KeyUp);
            // 
            // chkUseRef
            // 
            this.chkUseRef.AutoSize = true;
            this.chkUseRef.Location = new System.Drawing.Point(9, 300);
            this.chkUseRef.Name = "chkUseRef";
            this.chkUseRef.Size = new System.Drawing.Size(368, 29);
            this.chkUseRef.TabIndex = 15;
            this.chkUseRef.Text = "Use reference file (if it\'s available)";
            this.chkUseRef.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(216, 324);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 25);
            this.label3.TabIndex = 14;
            this.label3.Text = "Missiles";
            // 
            // nMissileCnt
            // 
            this.nMissileCnt.Location = new System.Drawing.Point(275, 322);
            this.nMissileCnt.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.nMissileCnt.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nMissileCnt.Name = "nMissileCnt";
            this.nMissileCnt.Size = new System.Drawing.Size(35, 31);
            this.nMissileCnt.TabIndex = 13;
            this.nMissileCnt.Value = new decimal(new int[] {
            9,
            0,
            0,
            0});
            // 
            // chkMissileCntRand
            // 
            this.chkMissileCntRand.AutoSize = true;
            this.chkMissileCntRand.Location = new System.Drawing.Point(9, 323);
            this.chkMissileCntRand.Name = "chkMissileCntRand";
            this.chkMissileCntRand.Size = new System.Drawing.Size(309, 29);
            this.chkMissileCntRand.TabIndex = 12;
            this.chkMissileCntRand.Text = "Random number of missiles";
            this.chkMissileCntRand.UseVisualStyleBackColor = true;
            this.chkMissileCntRand.CheckedChanged += new System.EventHandler(this.chkMissileCntRand_CheckedChanged);
            // 
            // chkAutoRestart
            // 
            this.chkAutoRestart.AutoSize = true;
            this.chkAutoRestart.Location = new System.Drawing.Point(117, 345);
            this.chkAutoRestart.Name = "chkAutoRestart";
            this.chkAutoRestart.Size = new System.Drawing.Size(155, 29);
            this.chkAutoRestart.TabIndex = 11;
            this.chkAutoRestart.Text = "Auto restart";
            this.chkAutoRestart.UseVisualStyleBackColor = true;
            // 
            // chkInfiniteMode
            // 
            this.chkInfiniteMode.AutoSize = true;
            this.chkInfiniteMode.Location = new System.Drawing.Point(9, 346);
            this.chkInfiniteMode.Name = "chkInfiniteMode";
            this.chkInfiniteMode.Size = new System.Drawing.Size(167, 29);
            this.chkInfiniteMode.TabIndex = 10;
            this.chkInfiniteMode.Text = "Infinite Mode";
            this.chkInfiniteMode.UseVisualStyleBackColor = true;
            // 
            // pnCrashProgress_Total
            // 
            this.pnCrashProgress_Total.BackColor = System.Drawing.Color.Black;
            this.pnCrashProgress_Total.Controls.Add(this.pnProgrssCrash_Guage);
            this.pnCrashProgress_Total.Location = new System.Drawing.Point(9, 9);
            this.pnCrashProgress_Total.Name = "pnCrashProgress_Total";
            this.pnCrashProgress_Total.Size = new System.Drawing.Size(306, 39);
            this.pnCrashProgress_Total.TabIndex = 6;
            // 
            // pnProgrssCrash_Guage
            // 
            this.pnProgrssCrash_Guage.BackColor = System.Drawing.Color.Red;
            this.pnProgrssCrash_Guage.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnProgrssCrash_Guage.Location = new System.Drawing.Point(0, 0);
            this.pnProgrssCrash_Guage.Name = "pnProgrssCrash_Guage";
            this.pnProgrssCrash_Guage.Size = new System.Drawing.Size(8, 39);
            this.pnProgrssCrash_Guage.TabIndex = 0;
            // 
            // lblCrashTotalCnt
            // 
            this.lblCrashTotalCnt.AutoSize = true;
            this.lblCrashTotalCnt.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblCrashTotalCnt.Location = new System.Drawing.Point(112, 57);
            this.lblCrashTotalCnt.Name = "lblCrashTotalCnt";
            this.lblCrashTotalCnt.Size = new System.Drawing.Size(33, 32);
            this.lblCrashTotalCnt.TabIndex = 5;
            this.lblCrashTotalCnt.Text = "0";
            // 
            // lblCrashRate
            // 
            this.lblCrashRate.AutoSize = true;
            this.lblCrashRate.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblCrashRate.Location = new System.Drawing.Point(112, 85);
            this.lblCrashRate.Name = "lblCrashRate";
            this.lblCrashRate.Size = new System.Drawing.Size(33, 32);
            this.lblCrashRate.TabIndex = 5;
            this.lblCrashRate.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.Location = new System.Drawing.Point(8, 57);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(199, 32);
            this.label4.TabIndex = 4;
            this.label4.Text = "Crash/Total:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.Location = new System.Drawing.Point(8, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(202, 32);
            this.label2.TabIndex = 4;
            this.label2.Text = "Crash Rate :";
            // 
            // btnStop
            // 
            this.btnStop.Enabled = false;
            this.btnStop.Location = new System.Drawing.Point(203, 368);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(112, 31);
            this.btnStop.TabIndex = 0;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // lblScore
            // 
            this.lblScore.AutoSize = true;
            this.lblScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblScore.Location = new System.Drawing.Point(111, 116);
            this.lblScore.Name = "lblScore";
            this.lblScore.Size = new System.Drawing.Size(58, 63);
            this.lblScore.TabIndex = 3;
            this.lblScore.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(8, 116);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(219, 63);
            this.label1.TabIndex = 2;
            this.label1.Text = "Score : ";
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem6});
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(99, 36);
            this.toolStripMenuItem4.Text = "도움말";
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(211, 38);
            this.toolStripMenuItem6.Text = "게임정보";
            this.toolStripMenuItem6.Click += new System.EventHandler(this.toolStripMenuItem6_Click);
            // 
            // mnu
            // 
            this.mnu.BackColor = System.Drawing.Color.White;
            this.mnu.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.mnu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem4,
            this.aISettingToolStripMenuItem,
            this.consoleToolStripMenuItem});
            this.mnu.Location = new System.Drawing.Point(0, 0);
            this.mnu.Name = "mnu";
            this.mnu.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.mnu.Size = new System.Drawing.Size(739, 40);
            this.mnu.TabIndex = 5;
            this.mnu.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.toolStripMenuItem3,
            this.toolStripSeparator1,
            this.끝내기ToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.ShortcutKeyDisplayString = "G";
            this.toolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.G)));
            this.toolStripMenuItem1.Size = new System.Drawing.Size(75, 36);
            this.toolStripMenuItem1.Text = "게임";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.toolStripMenuItem2.Size = new System.Drawing.Size(300, 38);
            this.toolStripMenuItem2.Text = "새게임";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(297, 6);
            // 
            // 끝내기ToolStripMenuItem
            // 
            this.끝내기ToolStripMenuItem.Name = "끝내기ToolStripMenuItem";
            this.끝내기ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.끝내기ToolStripMenuItem.Size = new System.Drawing.Size(300, 38);
            this.끝내기ToolStripMenuItem.Text = "끝내기";
            this.끝내기ToolStripMenuItem.Click += new System.EventHandler(this.끝내기ToolStripMenuItem_Click);
            // 
            // aISettingToolStripMenuItem
            // 
            this.aISettingToolStripMenuItem.Name = "aISettingToolStripMenuItem";
            this.aISettingToolStripMenuItem.Size = new System.Drawing.Size(128, 36);
            this.aISettingToolStripMenuItem.Text = "AI setting";
            this.aISettingToolStripMenuItem.Click += new System.EventHandler(this.aISettingToolStripMenuItem_Click);
            // 
            // consoleToolStripMenuItem
            // 
            this.consoleToolStripMenuItem.Name = "consoleToolStripMenuItem";
            this.consoleToolStripMenuItem.Size = new System.Drawing.Size(113, 36);
            this.consoleToolStripMenuItem.Text = "Console";
            this.consoleToolStripMenuItem.Click += new System.EventHandler(this.consoleToolStripMenuItem_Click);
            // 
            // frmmain
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(739, 432);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.mnu);
            this.MainMenuStrip = this.mnu;
            this.Name = "frmmain";
            this.Text = "Avoid Missile!!";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nMissileCnt)).EndInit();
            this.pnCrashProgress_Total.ResumeLayout(false);
            this.mnu.ResumeLayout(false);
            this.mnu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Timer timUpdate;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.MenuStrip mnu;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem 끝내기ToolStripMenuItem;
        private System.Windows.Forms.Label lblScore;
        private System.Windows.Forms.Label label1;
        private pnGame pnGameField;
        private System.Windows.Forms.Label lblCrashRate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel pnCrashProgress_Total;
        private System.Windows.Forms.Panel pnProgrssCrash_Guage;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Label lblCrashTotalCnt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chkInfiniteMode;
        private System.Windows.Forms.ToolStripMenuItem aISettingToolStripMenuItem;
        private System.Windows.Forms.CheckBox chkAutoRestart;
        private System.Windows.Forms.CheckBox chkMissileCntRand;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown nMissileCnt;
        private System.Windows.Forms.ToolStripMenuItem consoleToolStripMenuItem;
        private System.Windows.Forms.CheckBox chkUseRef;
    }
}

