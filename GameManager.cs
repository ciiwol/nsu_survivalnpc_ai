﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace survivalNPC_AI
{
    /// <summary>
    /// 게임매니저 클래스. 싱글톤.
    /// </summary>
    public class GameManager
    {
        const int NONE = -1, MISSILE = 0, WALL = 1;    //필드의 개체에 번호 부여

        #region public 속성

        /// <summary>
        /// 현재 게임이 진행중인지.
        /// </summary>
        public bool isGame = false;

        /// <summary>
        /// 게임 모드. true이면 1회 맞으면 죽는다. false이면 충돌시 충돌 횟수를 센다.
        /// </summary>
        public bool gamemodeDie = false;

        /// <summary>
        /// 자동 재시작. true이면 죽었을 경우 자동으로 다시시작.
        /// 임의로 중지한 경우는 다시시작하지 않는다.
        /// gamemodeDie가 false일 경우 무시한다.
        /// </summary>
        public bool autoRestart = false;

        /// <summary>
        /// 하드모드(벽에 닿으면 사망)?
        /// </summary>
        public bool isHardmode = true;

        /// <summary>
        /// 필드 크기 정의
        /// 해당 좌표에 어떤 개체가 있는지 대입
        /// </summary>
        public int[] fieldSize = new int[] { 400, 400 };
        public int[,] field;

        public Spaceship playerShip;
        public bool rndMissileCnt = false;  //random missile amount?
        public int missileCnt;  //missile amount
        public Missile[] missiles;
        public int score;   //tick total count
        public int hit;     //hit count

        //AI Classes
        public Simulate aiSimulator;

        #endregion

        #region private 속성
        
        //재시작시 학습지(참조파일)을 이용할 경우 여기에서 미사일 초기 속도를 가져온당
        private int[] misVelX = null;
        private int[] misVelY = null;
        #endregion

        /// <summary>
        /// 게임 매니저 생성자
        /// </summary>
        public GameManager()
        {
            field = new int[fieldSize[0], fieldSize[1]];
            
        }

        /// <summary>
        /// 게임 초기화
        /// </summary>
        public void Init(bool gamemodeDie = false, bool autoRestart = false, int missileCnt = 9, int[] missileVelX = null, int[] missileVelY = null)
        {
            this.gamemodeDie = gamemodeDie;
            this.autoRestart = autoRestart;
            rndMissileCnt = missileCnt == 0 ? true : false;
            this.missileCnt = missileCnt;

            if(rndMissileCnt) missileCnt = new Random().Next(5, 15);
            missiles = new Missile[missileCnt];

            //비행기 생성
            playerShip = new Spaceship(this);

            misVelX = missileVelX;
            misVelY = missileVelY;

            //미사일을 생성하기 전에 인자 상태 확인
            if(misVelX != null && misVelY != null)
                if(misVelX.Length != missileCnt || misVelY.Length != missileCnt)
                { misVelX = null; misVelY = null; }

            //미사일 생성 및 할당
            int[] yCandidates = { 20, 380 };
            for (int i = 0; i < missileCnt; i++)
            {
                if(misVelX == null)
                    missiles[i] = new Missile(this, i * 25, yCandidates[i % 2]);
                else
                    missiles[i] = new Missile(this, i * 25, yCandidates[i % 2], misVelX[i], misVelY[i]);
            }

            //변수 초기화
            score = 0;
            hit = 0;
            isGame = true;
            aiSimulator = null;
        }
        public void Init()
        {
            Init(gamemodeDie, autoRestart, missileCnt, misVelX, misVelY);
        }

        /// <summary>
        /// 게임매니저 Tick함수. pnGame이 그려질 때 마다 반복호출
        /// </summary>
        public void Tick()
        {
            //게임중이 아니면 Tick함수 작동안해
            if (!isGame) return;

            //우주선 Tick
            playerShip.Tick();

            //미사일 Tick
            foreach (Missile m in missiles)
            {
                m.Tick();
                #region 미사일 충돌체크
                //foreach (Missile m2 in missiles)
                //{
                //    if (m.Equals(m2)) continue;

                //    //x축 충돌
                //    //좌m 우m2
                //    if (m.x < m2.x && m.x + m.size + 2 >= m2.x)
                //        if (m.y < m2.y && m.y + m.size >= m2.y || m2.y <= m.y && m2.y + m.size >= m.y)
                //        {
                //            //부딪 -> x진행방향 반대로
                //            m.dirVector.X = -Math.Abs(m2.dirVector.X);
                //            m2.dirVector.X = Math.Abs(m.dirVector.X);
                //        }
                //    //좌m2 우m
                //    if (m.x >= m2.x && m.x <= m2.x + m.size + 2)
                //        if (m.y <= m2.y && m.y + m.size >= m2.y || m2.y <= m.y && m2.y + m.size >= m.y)
                //        {
                //            //부딪 -> x진행방향 반대로
                //            m.dirVector.X = Math.Abs(m2.dirVector.X);
                //            m2.dirVector.X = -Math.Abs(m.dirVector.X);
                //        }

                //    //y축 충돌
                //    //상m 하m2
                //    if (m.y <= m2.y && m.y + m.size - 2 >= m2.y)
                //        if (m.x <= m2.x && m.x + m.size >= m2.x || m2.x <= m.x && m2.x + m.size >= m.x)
                //        {
                //            //부딪 -> y진행방향 반대로
                //            m.dirVector.Y = -Math.Abs(m2.dirVector.Y);
                //            m2.dirVector.Y = Math.Abs(m.dirVector.Y);
                //        }

                //    //상m2 하m
                //    if (m.y >= m2.y && m.y <= m2.y + m.size - 2)
                //        if (m.x <= m2.x && m.x + m.size >= m2.x || m2.x <= m.x && m2.x + m.size >= m.x)
                //        {
                //            //부딪 -> y진행방향 반대로
                //            m.dirVector.Y = Math.Abs(m2.dirVector.Y);
                //            m2.dirVector.Y = -Math.Abs(m.dirVector.Y);
                //        }
                //}
                //미사일끼리 충돌체크 끝
                #endregion
            }

            //필드 배열 업데이트
            #region 필드 배열 갱신
            for(int y=0; y<fieldSize[1]; y++)
                for(int x=0; x<fieldSize[0]; x++)
                {
                    //빈공간
                    field[x, y] = NONE;

                    //미사일 위치
                    //foreach (Missile m in missiles)
                    //{
                    //    if (x >= m.x && x <= m.x + m.size)
                    //        if (y >= m.y && y <= m.y + m.size)
                    //            field[x, y] = MISSILE;
                    //}
                }
            foreach (Missile m in missiles)
            {
                 field[m.x + m.size/2, m.y + m.size/2] = MISSILE;
            }
            #endregion

            //AI 연동 플레이
            if (frmAI.aiPlay && !frmAI.isEvolving)
            {
                if (aiSimulator == null)
                    aiSimulator = new Simulate(this, frmAI.aiPath, 0, frmAI.aiThreshold);
                aiSimulator.simulateTick();
            }
            else
            {
                aiSimulator = null;
            }

            //점수계산
            score += 1;
        }

        /// <summary>
        /// 미사일과 비행기가 한번이라도 충돌시 호출되는 함수
        /// </summary>
        public void hitMissile()
        {
            //1회 맞으면 죽을 경우
            if(gamemodeDie)
            {
                //자동 재시작이 켜져있으면 자동으로 재시작
                if (autoRestart)
                {
                    Init();
                }
                else
                {
                    //게임 종료
                    isGame = false;
                }
            }

            //누적충돌일 경우
            else
            {
                hit += 1;
            }
        }


        /// <summary>
        /// 우주선 이동 요청 함수.
        /// </summary>
        /// <param name="dir">2, 4, 6, 8만 입력</param>
        /// <param name="on">true면 이동요청, false면 이동안함</param>
        public void controlSpaceship(int dir, bool on)
        {
            switch(dir)
            {
                case 2: playerShip.mov_D = on; break;
                case 4: playerShip.mov_L = on; break;
                case 6: playerShip.mov_R = on; break;
                case 8: playerShip.mov_U = on; break;
            }
        }

        /// <summary>
        /// frmmain에서 방향키로 우주선 이동을 요청한다.
        /// </summary>
        /// <param name="k">키. D, A, W, S만 입력</param>
        /// <param name="on">true는 이동요청, false는 이동종료</param>
        public void controlSpaceship(System.Windows.Forms.Keys k, bool on)
        {
            int dir = 5;
            switch(k)
            {
                case System.Windows.Forms.Keys.D: dir = 6; break;
                case System.Windows.Forms.Keys.A: dir = 4; break;
                case System.Windows.Forms.Keys.W: dir = 8; break;
                case System.Windows.Forms.Keys.S: dir = 2; break;
            }

            if (dir == 5) return;
            controlSpaceship(dir, on);
        }
    }
}
