﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace survivalNPC_AI
{
    public class Missile
    {
        #region public 속성

        public int x
        {
            get { return X; }
            set
            {
                if (value < 0) { X = 0; dirVector.X = Math.Abs(dirVector.X); }
                else if (value >= gm.fieldSize[0] - size) { X = gm.fieldSize[0] - size - 1; dirVector.X = -Math.Abs(dirVector.X); }
                else X = value;
            }
        }
        public int y
        {
            get { return Y; }
            set
            {
                if (value < 0) { Y = 0; dirVector.Y = Math.Abs(dirVector.Y); }
                else if (value >= gm.fieldSize[1] - size) { Y = gm.fieldSize[1] - size - 1; dirVector.Y = -Math.Abs(dirVector.Y); }
                else Y = value;
            }
        }

        public int size = 15;

        /// <summary>
        /// 방향벡터(Direction Vector)
        /// </summary>
        public Point dirVector;

        #endregion

        #region private 속성

        private int X;
        private int Y;
        Image img;
        GameManager gm;

        /// <summary>
        /// 미사일이 현재 충돌'중'인지 판단.
        /// 충돌이 시작될 때 true가 되고, true상태에선 충돌처리를 하지 않는다.
        /// 충돌이 벗어날 때 다시 false가 된다.
        /// 이 변수를 통해 비행기가 미사일과 충돌중일 때 충돌횟수가 지나치게 카운트되는 것을 피할 수 있다.
        /// </summary>
        bool onCrash = false;

        #endregion

        /// <summary>
        /// 미사일 생성자
        /// </summary>
        public Missile(GameManager gm,  int locationX = 0, int locationY = 0, int velX = 0, int velY = 0)
        {
            this.gm = gm;
            Random rnd = new Random(DateTime.Now.Millisecond * locationX);
            if (!frmAI.isEvolving) img = Image.FromFile("gong.gif");
            if(velX == 0 && velY == 0)
                dirVector = new Point(rnd.Next(1, 4), rnd.Next(1, 4));
            else
                dirVector = new Point(velX, velY);


            x = locationX;
            y = locationY;
        }

        public Image getImage()
        { return img; }

        /// <summary>
        /// 미사일 Tick.
        /// pnGame 페인트 될 때마다 반복호출
        /// </summary>
        public void Tick()
        {
            //미사일 이동
            x += dirVector.X;
            y += dirVector.Y;

            //미사일 벽 충돌시 각도변경 -> setter에서 한다.

            //미사일끼리 충돌은 게임매니저(GameManager)에서 설정

            //NPC와 미사일 충돌체크
            if(!onCrash)
            if(x <= gm.playerShip.x && x + size - 8 >= gm.playerShip.x ||
                x <= gm.playerShip.x + gm.playerShip.width - 8 && x >= gm.playerShip.x)
            {
                if(y <= gm.playerShip.y && y + size - 9 >= gm.playerShip.y ||
                    y <= gm.playerShip.y + gm.playerShip.height - 7 && y >= gm.playerShip.y)
                {
                    //비행기가 이 미사일의 범위 안에 있으면 충돌함수 호출
                    HitMissile();
                }
                 else onCrash = false;
            }
            else onCrash = false;

        }

        /// <summary>
        /// 미사일과 비행기가 충돌시 호출되는 함수
        /// </summary>
        void HitMissile()
        {
            onCrash = true;
            //충돌시 게임매니저 클래스의 HitMissile함수 호출
            //게임매니저는 싱글톤이기때문에 델리게이트 필요없을듯ㅋ
            gm.hitMissile();
        }
    }
}
