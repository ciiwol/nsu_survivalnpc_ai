﻿//      Survival NPC AI ver1.0

//  이 코드는 내가 다 뜯어고치도록 하지.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.Media;
using System.IO;

namespace survivalNPC_AI
{
    public partial class frmmain : Form
    {

        public GameManager gm;
        frmAI aiForm;

        public frmmain()
        {
            InitializeComponent();
            gm = new GameManager();
        }
    

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        //시작버튼 -> 게임 시작                                                     
        private void start_Click(object sender, EventArgs e)
        {
            //진화중일경우 게임 X
            if (frmAI.isEvolving)
            {
                MessageBox.Show("Evolution process is running now.");
                return;
            }

           init(); // 게임 시작전 기본 변수들 초기화
            

            btnStart.Enabled = false; //스타트 버튼 비활성화
            btnStop.Enabled = true;

            gm.isGame = true;   //게임 시작
            timUpdate.Enabled = true; //타이머 작동
        }

        //멈춤버튼 -> 게임 중지
        private void btnStop_Click(object sender, EventArgs e)
        {
            gm.isGame = false; //게임 종료
        }


        public void init()  //초기 값들 초기화
        {
            //참조파일 사용?
            if (chkUseRef.Checked && frmAI.aiPath != "")
            {
                //참조파일(학습지) 불러오기
                int mNumber = 0;
                int[] mvX = null, mvY = null;
                string[] fParams = File.ReadAllText(string.Format("{0}_ref", frmAI.aiPath)).Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                mNumber = Convert.ToInt16(fParams[0]);
                mvX = new int[mNumber]; mvY = new int[mNumber];
                for (int i = 0; i < mNumber; i++)
                {
                    mvX[i] = Convert.ToInt16(fParams[i+1]);
                    mvY[i] = Convert.ToInt16(fParams[i + 1 + mNumber]);
                }
                gm.Init(!chkInfiniteMode.Checked,
                    chkAutoRestart.Checked,
                    mNumber, mvX, mvY
                    );     // 게임매니저 재초기화
            }
            else
                gm.Init(!chkInfiniteMode.Checked,
                        chkAutoRestart.Checked,
                        chkMissileCntRand.Checked ? 0 : (int)nMissileCnt.Value
                        );     // 게임매니저 재초기화

            //컨트롤 비활성
            chkInfiniteMode.Enabled = false;
            chkAutoRestart.Enabled = false;
            chkMissileCntRand.Enabled = false;
            chkUseRef.Enabled = false;
            nMissileCnt.Enabled = false;
        }
    
        private void timer1_Tick(object sender, EventArgs e)
        {
            //게임중이 아니면 타이머 종료
            if(!gm.isGame)
            {
                gameOver();
                return;
            }

            //게임 Tick 진행
            gm.Tick();

            //필드 업데이트
            pnGameField.Refresh();
            
            if(gm.gamemodeDie)
            {
                //점수 표시 갱신
                lblScore.Text = gm.score.ToString();
            }
            else
            {            
                //게이지 계산
                lblCrashRate.Text = string.Format("{0:0.00}%", (gm.hit / (float)gm.score * 100));
                pnProgrssCrash_Guage.Width = (int)((gm.hit * pnCrashProgress_Total.Width) / (float)gm.score * 10);
                lblCrashTotalCnt.Text = string.Format("{0}/{1}", gm.hit, gm.score);
            }
        }
        
        /// <summary>
        /// 게임오버시 폼의 컨트롤 상태 설정
        /// </summary>
        void gameOver()
        {
            timUpdate.Enabled = false;
            btnStart.Enabled = true;
            btnStop.Enabled = false;

            //컨트롤 활성
            chkInfiniteMode.Enabled = true;
            chkAutoRestart.Enabled = true;
            chkMissileCntRand.Enabled = true;
            chkUseRef.Enabled = true;
            if(!chkMissileCntRand.Checked) nMissileCnt.Enabled = true;
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            init();
            Graphics g = this.CreateGraphics();
            Brush brush = new SolidBrush(Color.White);
            g.FillRectangle(brush, 100, 100, 400, 400);
                       
        }

        private void 끝내기ToolStripMenuItem_Click(object sender, EventArgs e)
        { Application.Exit(); }

        private void splitContainer1_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pnGame1_Paint(object sender, PaintEventArgs e)
        {
            if (!gm.isGame) return;

            Graphics g = e.Graphics;
            Pen pen = new Pen(Color.Black);
 
            //검은색 배경
            g.DrawRectangle(pen, new Rectangle(0, 0, 400, 400));

            //플레이어 우주선 그리기
            g.DrawImage(gm.playerShip.getImage(), gm.playerShip.x, gm.playerShip.y, gm.playerShip.width, gm.playerShip.height);

            //미사일들 그리기
            foreach(Missile m in gm.missiles)
                g.DrawImage(m.getImage(), m.x, m.y, m.size, m.size);

        }

        private void toolStripMenuItem6_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Avoid Missile + AI" + Environment.NewLine +
                    "Jujin Kim, Jongmin Park, Junho Park, Jongseong Lee" + Environment.NewLine +
                    "Computer Science, Namseoul Univ.");
        }

        private void pnGame1_KeyUp(object sender, KeyEventArgs e)
        { gm.controlSpaceship(e.KeyCode, false); }

        private void pnGame1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape) { Application.Exit(); }
            gm.controlSpaceship(e.KeyCode, true);
        }

        private void chkAIPlay_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void aISettingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (aiForm == null || aiForm.IsDisposed)
                aiForm = new frmAI(gm);
            aiForm.Hide();
            aiForm.Show();
            aiForm.Left = this.Right + 1;
            aiForm.Top = this.Top;
        }

        private void chkMissileCntRand_CheckedChanged(object sender, EventArgs e)
        {
            if (chkMissileCntRand.Checked)
                nMissileCnt.Enabled = false;
            else
                nMissileCnt.Enabled = true;
        }

        private void consoleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmConsole.getInstance.Hide();
            frmConsole.getInstance.Show();
            frmConsole.getInstance.Left = this.Left;
            frmConsole.getInstance.Top = this.Bottom + 1;
        }
    }
}